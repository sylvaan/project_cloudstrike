import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NewsDetail extends StatefulWidget {
  List? data;
  NewsDetail({Key? key, required this.data}) : super(key: key);

  @override
  _NewsDetailState createState() => _NewsDetailState(data);
}

class _NewsDetailState extends State<NewsDetail> {
  List? data;
  _NewsDetailState(this.data);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red[700],
        title: Text("House Details"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(4),
            child: TextButton(
              onPressed: () {
                Get.back();
              },
              child: Icon(
                Icons.exit_to_app,
                color: Colors.white,
              ),
            ),
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "House Name: " + data![0],
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
            ),
            SizedBox(
              height: 12,
            ),
            Text(
              "Region: " + data![1],
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
            ),
            SizedBox(
              height: 12,
            ),
            Text(
              "Coat of Arms: " + data![2],
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
            ),
            SizedBox(
              height: 12,
            ),
            Text(
              "Words: " + data![3],
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
            ),
          ],
        ),
      ),
    );
  }
}
