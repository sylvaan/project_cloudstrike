import 'dart:async';

import 'package:cloudstrike/screens/loginscreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisScreen extends StatefulWidget {
  const RegisScreen({Key? key}) : super(key: key);

  @override
  _RegisScreenState createState() => _RegisScreenState();
}

class _RegisScreenState extends State<RegisScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passController = TextEditingController();
  FirebaseAuth fauth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          padding: EdgeInsets.all(32.0),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(2),
                  child: Image.asset("assets/images/CLWN.png"),
                ),
                Container(
                  padding: EdgeInsets.all(2),
                  child: Text(
                    "Register Page",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(2),
                  child: TextFormField(
                      controller: emailController,
                      decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(1.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                                style: BorderStyle.solid,
                              )),
                          prefixIcon: Icon(Icons.person),
                          hintText: "Email")),
                ),
                Container(
                  padding: EdgeInsets.all(2),
                  child: TextFormField(
                      controller: passController,
                      decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(1.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                                style: BorderStyle.solid,
                              )),
                          prefixIcon: Icon(Icons.password),
                          hintText: "Password")),
                ),
                Container(
                    padding: EdgeInsets.all(2),
                    child: ElevatedButton(
                      child: Text("Register"),
                      onPressed: () async {
                        final sb;
                        try {
                          await fauth.createUserWithEmailAndPassword(
                              email: emailController.text,
                              password: passController.text);
                        } catch (error) {
                          print(error);
                        }
                        sb = SnackBar(
                            content: Text(
                                "Registration Successful. You will be redirected to Login Page"));
                        ScaffoldMessenger.of(context).showSnackBar(sb);
                        Timer(
                            Duration(seconds: 4), () => Get.to(LoginScreen()));
                      },
                    ))
              ],
            ),
          )),
    );
  }
}
