import 'package:cloudstrike/screens/mainscreen.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passController = TextEditingController();
  FirebaseAuth fauth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          padding: EdgeInsets.all(32.0),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(2),
                  child: Image.asset("assets/images/CLWN.png"),
                ),
                Container(
                  padding: EdgeInsets.all(2),
                  child: Text(
                    "Login Page",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(2),
                  child: TextFormField(
                      controller: emailController,
                      decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(1.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                                style: BorderStyle.solid,
                              )),
                          prefixIcon: Icon(Icons.person),
                          hintText: "Email")),
                ),
                Container(
                  padding: EdgeInsets.all(2),
                  child: TextFormField(
                      controller: passController,
                      obscureText: true,
                      decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(1.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                                style: BorderStyle.solid,
                              )),
                          prefixIcon: Icon(Icons.password),
                          hintText: "Password")),
                ),
                Container(
                    padding: EdgeInsets.all(2),
                    child: ElevatedButton(
                      child: Text("Login"),
                      onPressed: () async {
                        await fauth.signInWithEmailAndPassword(
                            email: emailController.text,
                            password: passController.text);
                        final sb = SnackBar(content: Text("Login Successful"));
                        ScaffoldMessenger.of(context).showSnackBar(sb);
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => MainScreen()));
                      },
                    ))
              ],
            ),
          )),
    );
  }
}
