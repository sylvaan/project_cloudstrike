import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  var user = FirebaseAuth.instance.currentUser;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red[700],
        title: Text("Home Page"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(4),
            child: TextButton(
              onPressed: () {
                Get.back();
              },
              child: Icon(
                Icons.exit_to_app,
                color: Colors.white,
              ),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(32),
          child: Container(
            padding: EdgeInsets.all(1),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 40,
                  child: RichText(
                    text: TextSpan(
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                            color: Colors.blue[600]),
                        children: <TextSpan>[
                          TextSpan(text: "Hello, "),
                          TextSpan(
                              text: user!.email!
                                  .substring(0, user!.email!.indexOf("@")),
                              style: TextStyle(
                                  color: Colors.blue[200])),
                        ]),
                  ),
                ),
                Container(
                  height: 40,
                  child: RichText(
                    text: TextSpan(
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                            color: Colors.blue[600]),
                        children: <TextSpan>[
                          TextSpan(text: "Welcome "),
                          TextSpan(text: "to "),
                          TextSpan(
                              text: "Cloudstrike",
                              style: TextStyle(
                                  color: Colors.red.shade800)),
                        ]),
                  ),
                ),
                Container(
                  height: 90,
                  padding: EdgeInsets.all(2),
                  child: TextFormField(
                      decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(1.0),
                              borderSide: BorderSide(
                                color: Colors.black,
                                style: BorderStyle.solid,
                              )),
                          prefixIcon: Icon(Icons.search),
                          hintText: "Search")),
                ),
                Container(
                  height: 20,
                  child: Text("Pick your poison"),
                ),
                Container(
                  height: 300,
                  child: GridView.count(
                    crossAxisCount: 2,
                    crossAxisSpacing: 2,
                    mainAxisSpacing: 3,
                    children: <Widget>[
                      SizedBox(
                        height: 50,
                        width: 40,
                        child: Image.asset(
                          "assets/images/leviathan.jpg",
                          fit: BoxFit.contain,
                        ),
                      ),
                      SizedBox(
                        height: 50,
                        width: 40,
                        child: Image.asset(
                          "assets/images/lw.jpg",
                          fit: BoxFit.contain,
                        ),
                      ),
                      SizedBox(
                        height: 50,
                        width: 40,
                        child: Image.asset(
                          "assets/images/gos.png",
                          fit: BoxFit.contain,
                        ),
                      ),
                      SizedBox(
                        height: 50,
                        width: 40,
                        child: Image.asset(
                          "assets/images/dsc.jpg",
                          fit: BoxFit.contain,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
