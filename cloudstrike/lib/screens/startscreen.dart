import 'package:cloudstrike/screens/loginscreen.dart';
import 'package:cloudstrike/screens/registerscreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class StartScreen extends StatefulWidget {
  
  const StartScreen({ Key? key }) : super(key: key);

  @override
  _StartScreenState createState(
    
  ) => _StartScreenState();
}

class _StartScreenState extends 
State<StartScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          padding: EdgeInsets.all(32.0),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(2),
                  child: Image.asset("assets/images/CLWN.png"),
                ),
                Container(
                  child: RichText(text: TextSpan(
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Colors.black),
                    children: <TextSpan>[
                      TextSpan(text: "Welcome "),
                      TextSpan(text: "to "),
                      TextSpan(text: "Cloudstrike", style: TextStyle(color: Colors.red.shade800)),
                    ]
                  ),),
                ),
                Container(
                  padding: EdgeInsets.only(left: 10, top: 10, right: 10),
                  child: ElevatedButton(
                    child: Text("Register"),
                    onPressed: (){
                      Get.to(RegisScreen());
                    },
                  )
                ),
                Container(
                  padding: EdgeInsets.only(left: 10, bottom: 10, right: 10),
                  child: ElevatedButton(
                    child: Text("Login"),
                    onPressed: (){
                      Get.to(LoginScreen());
                    },
                  )
                )
              ],
            ),
          )),
    );
  }
}