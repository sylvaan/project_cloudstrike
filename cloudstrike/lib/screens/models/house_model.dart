import 'dart:convert';

House HousefromJson(String s) => House.fromJson(json.decode(s));
String HousetoJson(House h) => json.encode(h.toJson());

class House {
  House({
    required this.name,
    required this.region,
    required this.coatOfArms,
    required this.words,
  });

  String name;
  String region;
  String coatOfArms;
  String words;

  factory House.fromJson(Map<String, dynamic> json) => House(
      name: json["name"],
      region: json["region"],
      coatOfArms: json["coatOfArms"],
      words: json["words"]);

  Map<String, dynamic> toJson() => {
        "name": name,
        "region": region,
        "coatOfArms": coatOfArms,
        "words": words
      };
}
