import 'dart:async';
import 'package:flutter/material.dart';
import 'startscreen.dart';
import 'package:get/get.dart';

class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 4),
        () => Get.off(StartScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(2),
              child: Image.asset("assets/images/CLWN.png"),
            ),
            Container(
              padding: EdgeInsets.all(2),
              child: Text("You are being redirected..."),
            )
          ],
        ),
      ),
    );
  }
}
