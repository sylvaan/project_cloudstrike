import 'dart:convert';
import 'package:cloudstrike/screens/models/house_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:cloudstrike/screens/newsdetail.dart';

class NewsScreen extends StatefulWidget {
  const NewsScreen({ Key? key }) : super(key: key);

  @override
  _NewsScreenState createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {

  final String url = "https://www.anapioficeandfire.com/api/houses";
  List<House> h = [];

  @override
  void initState(){
    _getRefreshData();
    super.initState();
  }

  Future<void> _getRefreshData() async{
    this.getJsonData(context);
  }

  Future getJsonData(BuildContext Context) async{
    var resp = await http.get(Uri.parse(url));

    var jsonData = jsonDecode(resp.body);
    print(resp.body);
    for (var i in jsonData){
      House ho = House(name: i["name"], region: i["region"], coatOfArms: i["coatOfArms"], words: i["words"]);
      h.add(ho);
    }
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red[700],
        title: Text("View Page"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(4),
            child: TextButton(
              onPressed: (){
                Get.back();
              },
              child: Icon(Icons.exit_to_app, color: Colors.white,),
              
            ),
          )
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _getRefreshData,
        child: ListView.builder(
          itemCount: h.length,
          itemBuilder: (BuildContext context, int index){
            return Container(
              margin: EdgeInsets.all(5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  GestureDetector(
                    onTap: (){
                      var d1 = h[index].name;
                      var d2 = h[index].region;
                      var d3 = h[index].coatOfArms;
                      var d4 = h[index].words;
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                            NewsDetail(data: [d1, d2, d3, d4])));
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            height: 5,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                h[index].name,
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w700),
                              ),
                              Icon(Icons.chevron_right),
                            ],
                          ),

                          SizedBox(
                            height: 3,
                          ),
                          Divider(
                            height: 20,
                            thickness: 1,
                            endIndent: 25,
                            indent: 15,
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            );
          }),
      ),
    );
  }
}