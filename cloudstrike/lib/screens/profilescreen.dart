import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({ Key? key }) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  var user = FirebaseAuth.instance.currentUser;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red[700],
        title: Text("Profile Page"),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(4),
            child: TextButton(
              onPressed: (){
                Get.back();
              },
              child: Icon(Icons.exit_to_app, color: Colors.white,),
              
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(32),
          child: Container(
            padding: EdgeInsets.all(1),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: CircleAvatar(
                    radius: 100,
                    backgroundColor: Colors.deepOrange[400],
                    child: CircleAvatar(
                      radius: 90,
                      backgroundImage: AssetImage("assets/images/jotunn.jpg"),
                    ),
                  )
                ),
                Container(
                  margin: EdgeInsets.only(top: 25),
                  height: 20,
                  child: Text(user!.email!.substring(0, user!.email!.indexOf("@"))),
                ),
                Container(
                  height: 20,
                  child: Text(user!.email.toString()),
                ),
                Container(
                  margin: EdgeInsets.only(top: 35),
                  height: 30,
                  child: Text("My Poison", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                ),
                Container(
                  height: 1000,
                  child: GridView.count(
                    crossAxisCount: 2,
                    crossAxisSpacing: 2,
                    mainAxisSpacing: 3,
                    children: <Widget>[
                      SizedBox(
                        height: 50,
                        width: 40,
                        child: Image.asset(
                          "assets/images/leviathan.jpg",
                          fit: BoxFit.contain,
                        ),
                      ),
                      SizedBox(
                        height: 50,
                        width: 40,
                        child: Image.asset(
                          "assets/images/lw.jpg",
                          fit: BoxFit.contain,
                        ),
                      ),
                      SizedBox(
                        height: 50,
                        width: 40,
                        child: Image.asset(
                          "assets/images/gos.png",
                          fit: BoxFit.contain,
                        ),
                      ),
                      SizedBox(
                        height: 50,
                        width: 40,
                        child: Image.asset(
                          "assets/images/dsc.jpg",
                          fit: BoxFit.contain,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}