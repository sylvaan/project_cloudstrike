import 'package:cloudstrike/screens/homescreen.dart';
import 'package:cloudstrike/screens/newsscreen.dart';
import 'package:cloudstrike/screens/profilescreen.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatefulWidget {
  MainScreen({ Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int tab = 0;
  final List<Widget> screens = [
    HomeScreen(),
    NewsScreen(),
    ProfileScreen(),
  ];

  final PageStorageBucket psb = PageStorageBucket();
  Widget currscreen = HomeScreen();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:  PageStorage(
        child: currscreen,
        bucket: psb,
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 10,
        child: Container(
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currscreen = HomeScreen();
                        tab = 0;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.home_filled,
                        color: tab == 0 ? Colors.pink : Colors.blue[200],),
                        Text("Home", style: TextStyle(color: tab == 0 ? Colors.pink : Colors.blue[200],),),
                      ],
                    ),
                    ),
                    MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currscreen = NewsScreen();
                        tab = 1;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.grid_view_outlined,
                        color: tab == 1 ? Colors.pink : Colors.blue[200],),
                        Text("View", style: TextStyle(color: tab == 1 ? Colors.pink : Colors.blue[200],),),
                      ],
                    ),
                    ),
                    MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currscreen = ProfileScreen();
                        tab = 2;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.account_box,
                        color: tab == 2 ? Colors.pink : Colors.blue[200],),
                        Text("Account", style: TextStyle(color: tab == 2 ? Colors.pink : Colors.blue[200],),),
                      ],
                    ),
                    )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}